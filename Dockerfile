ARG BASE_IMAGE=rabbitmq
ARG VERSION=3.11-management

FROM $BASE_IMAGE:$VERSION

COPY --chown=999:999 ./advanced.config /etc/rabbitmq/advanced.config
COPY --chown=999:999 ./rabbitmq.conf /etc/rabbitmq/rabbitmq.conf
COPY --chown=999:999 ./enabled_plugins /etc/rabbitmq/enabled_plugins
COPY --chown=999:999 ./definitions.json /etc/rabbitmq/definitions.json
