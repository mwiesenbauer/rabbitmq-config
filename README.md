# RabbitMQ Configuration Example

RabbitMQ Configuration example with MQTT und mTLS (aka. client certificate auth).

Device clients can only publish and subscribe to certain topics with their username in it.
Backend has access to everything.

# Systemd

[system-protection.conf](system-protection.conf) is a systemd drop-in unit file.
Place it under `/etc/systemd/system/rabbitmq-server.service.d/`
