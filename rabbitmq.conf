log.console = true
log.console.level = info

log.syslog = false
log.syslog.level = info

log.file = false
log.file.level = info

auth_backends.1 = rabbit_auth_backend_internal
auth_mechanisms.1 = EXTERNAL

# ssl config
ssl_options.versions.1 = tlsv1.2

ssl_options.honor_cipher_order = true
ssl_options.honor_ecc_order = true

# remove the following two lines for tls 1.3
ssl_options.client_renegotiation = false
ssl_options.secure_renegotiate = true

ssl_options.verify = verify_peer
ssl_options.fail_if_no_peer_cert = true
ssl_cert_login_from = common_name

ssl_options.cacertfile = /etc/rabbitmq/chain.pem
ssl_options.certfile = /etc/rabbitmq/server.pem
ssl_options.keyfile = /etc/rabbitmq/server-key.pem

ssl_options.ciphers.1 = ECDHE-ECDSA-AES256-GCM-SHA384
ssl_options.ciphers.2 = ECDHE-RSA-AES256-GCM-SHA384
ssl_options.ciphers.3 = ECDHE-ECDSA-AES256-SHA384
ssl_options.ciphers.4 = ECDHE-RSA-AES256-SHA384
ssl_options.ciphers.5 = ECDH-ECDSA-AES256-GCM-SHA384
ssl_options.ciphers.6 = ECDH-RSA-AES256-GCM-SHA384
ssl_options.ciphers.7 = ECDH-ECDSA-AES256-SHA384
ssl_options.ciphers.8 = ECDH-RSA-AES256-SHA384
ssl_options.ciphers.9 = DHE-RSA-AES256-GCM-SHA384

# if you can use tls 1.3
#ssl_options.versions.1 = tlsv1.3

#ssl_options.ciphers.1  = TLS_AES_256_GCM_SHA384
#ssl_options.ciphers.2  = TLS_AES_128_GCM_SHA256
#ssl_options.ciphers.3  = TLS_CHACHA20_POLY1305_SHA256
#ssl_options.ciphers.4  = TLS_AES_128_CCM_SHA256
#ssl_options.ciphers.5  = TLS_AES_128_CCM_8_SHA256

# mqtt plugin
## use ETS in-memory store for retained messages
mqtt.retained_message_store = rabbit_mqtt_retained_msg_store_ets

mqtt.allow_anonymous = false

mqtt.listeners.tcp = none
mqtt.listeners.ssl.default = 8883

mqtt.vhost = /
mqtt.exchange = amq.topic
mqtt.subscription_ttl = 86400000

mqtt.prefetch = 10
mqtt.ssl_cert_login = true
mqtt.proxy_protocol = false

mqtt.tcp_listen_options.keepalive = true
mqtt.tcp_listen_options.nodelay = true

# management plugin
management.tcp.port = 15672
management.tcp.ip = 127.0.0.1
management.ssl.port = 15671
management.load_definitions = /etc/rabbitmq/definitions.json

# https://www.rabbitmq.com/management.html#disable-stats
# tl;dr; disabling internal metric collection is recommended
management_agent.disable_metrics_collector = true

# prometheus
prometheus.tcp.port = 15692
prometheus.tcp.ip = 127.0.0.1
